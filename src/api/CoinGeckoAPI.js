const config = {
    baseURL: 'https://api.coingecko.com/api/v3'
  }

const entities = {
    'coins': {
        keys: ['id', 'marketcap', 'current_price'],
        keyId: 'id'
    }
}

const makeAPI = (entities, config) => {
    const {
      baseURL
    } = config
    return Object.keys(entities).reduce((API, entityKey) => ({
      ...API,
      [entityKey]: {
        get: () => fetch(
          `${baseURL}/${entityKey}/list`,
          { method: 'GET' }
        ),
        getOne: (id) => fetch(
          `${baseURL}/${entityKey}/${id}`,
          { method: 'GET' }
        )
      }
    }), {})
  }

export const API = makeAPI(entities, config)
