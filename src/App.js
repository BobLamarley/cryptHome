import React, { useState } from "react";
import CryptoList from './components/cryptoList/cryptoList';
import DefaultLayout from './components/defaultLayout/defaultLayout';
import Login from './components/login/login';
import CryptoHook from './components/cryptoHook/cryptoHook';
import UserList from './components/userList/userList';
import CreateAccount from './components/createAccount/createAccount';
import CryptoDetail from './components/cryptoDetail/cryptoDetail';
import { Provider } from 'react-redux'
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Home from './components/home/home';
import { AuthContext } from './context';
import './components/menu/menu.css';
import './App.css';
import configureStore from './store/configureStore';
import history from './history';

const store = configureStore();

export default function App () {

    const [authenticated, setAuthenticated] = useState(true) // For dev, true in order to be always connected
  
    return (
        <Provider store={store}>
            <AuthContext.Provider
                value={{ authenticated, setAuthenticated }}
            >
                <Router history={history}>
                    <div>
                        <DefaultLayout exact path="/" component={Home} />
                        <DefaultLayout path="/cryptolist" component={CryptoList} /> 
                        <DefaultLayout path="/cryptohook" component={CryptoHook} />
                        <DefaultLayout path="/userList" component={UserList} />
                        <DefaultLayout path="/cryptoDetail/:cryptoId" component={CryptoDetail} />
                        <Route path="/login" component={Login}/>
                        <Route path="/createAccount" component={CreateAccount} />
                    </div>    
                </Router>
            </AuthContext.Provider>    
        </Provider>    
    )
  }
