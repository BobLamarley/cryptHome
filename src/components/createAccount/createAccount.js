import React from "react";

import { Box, Button, CheckBox, Grommet, Form, FormField, RangeInput, Select } from "grommet";
import { grommet } from "grommet/themes";

export default function createAccount(){
    const [age, setAge] = React.useState(18);
   

      return  <Grommet full theme={grommet}>
      <Box fill align="center" justify="center">
        <Box width="medium">
          <Form
            onReset={event => console.log(event)}
            onSubmit={({ value }) => console.log("Submit", value)}
          >
            <center>Formulaire d'inscription</center>
            <FormField
              label="Pseudo"
              name="pseudo"
              required
              validate={{ regexp: /^[a-z]/i }}
            />
            <FormField label="Email" name="email" type="email" required />
            <FormField
              name="newsletterSubscription"
              component={CheckBox}
              pad
              label="S'inscrire à la newsletter?"
            />
            <FormField
              label="Capital détenu"
              name="size"
              component={Select}
              options={["1k-10k€", "10k-100k€", "+100k€"]}
            />
            <FormField
              label={`Age: ${age}`}
              name="age"
              component={RangeInput}
              pad
              value={age}
              onChange={event => setAge(event.target.value)}
              min={18}
              max={99}
            />
            <Box direction="row" justify="between" margin={{ top: "medium" }}>
              <Button label="Retour" />
              <Button type="reset" label="Remise à 0" />
              <Button type="submit" label="Envoyer" primary />
            </Box>
          </Form>
        </Box>
      </Box>
    </Grommet>
      
}
