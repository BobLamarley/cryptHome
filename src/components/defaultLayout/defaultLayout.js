import React, { useContext } from "react";
import '../home/home.css';
import Menu from '../menu/menu';
import './defaultLayout.css';
import '../menu/menu.css';
import { Link, Redirect } from "react-router-dom";
import { Route } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';
import { AuthContext } from '../../context';
import { Container, Row, Col, Button } from 'react-bootstrap';


export default function DefaultLayout ({component: MatchedPage, ...rest}) {
  const { authenticated, setAuthenticated } = useContext(AuthContext)
  console.log("authent dans defaultLayout" + authenticated);
  return (

      <Route {...rest} render={ (matchProps) => authenticated === true
        ? (
          <Container fluid className="noPadding">
            <Row className="justify-content-md-center navbarRow stickyNav">
              <Navbar id="logout">
                <Button variant="dark" onClick={() => setAuthenticated(false)}>Logout</Button>
              </Navbar>
            </Row>  
            <Row>
              <Menu>
                <Link className="menuLink" to="/"><i className="fa fa-fw fa-database" /><span> Home</span></Link>
                <Link className="menuLink" id="cryptoListe" to="/cryptolist"><i className="fa fa-fw fa-map-marker" /><span> Cryptolist</span></Link>
                <Link className="menuLink" id="cryptoHook" to="/cryptohook"><i className="fa fa-fw fa-mortar-board" /><span> Cryptohook</span></Link>
                <Link className="menuLink" id="userList" to="/userList"><i className="fa fa-fw fa-picture-o" /><span> UserList</span></Link>
              </Menu>
            </Row>
            <Row>
              <Col md={{ offset: 3, span: 6  }}>
                <MatchedPage {...matchProps} />
              </Col>  
            </Row>     
          </Container>
      )
      :  <Redirect to={{pathname: '/login', state: {from: matchProps.location}}} />
    } />
  )
};