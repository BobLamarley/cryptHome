import React, { useState, useEffect, useMemo } from 'react';
import axios from 'axios';
import './cryptoHook.css';
import { Link} from "react-router-dom";
import { Form, FormControl, Button, Card } from 'react-bootstrap';
import Loader from 'react-loader-spinner';



export default function CryptoHook() {
  const [list, setList] = useState([])
  const [search, setSearch] = useState('')

  const fetch = async () => {
    const response = await fetchCrypto()
    setList(response.data)
  }

  useEffect(() => {
    setTimeout(() => fetch(), 2000)
  }, [])

  if (list && !list.length) {
    return (
      <div>
        <Loader
          type="Ball-Triangle"
          color="#00BFFF"
          height="100"	
          width="100"/>
      </div>  

        )
  }

  return (
    <div id="cryptoHook">
      <div id="searchInCryptoInput">
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={({ target: { value } }) => { setSearch(value) }} />
        </Form>
      </div>
      <CryptoList list={list} search={search} />
    </div>
  )
}

async function fetchCrypto() {
  return await axios.get(
   'https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur'
  )
}

const CryptoList = (props) => {
  const {
    list = [],
    search,
  } = props

  const filteredList = useMemo(() => {
    if (!search.length) return list    
    return list.filter(({ name }) => -1 !== name.indexOf(search))
  }, [list, search])

  return (
    <div id="cryptoList"> 
      <ul>
        {filteredList.map(((crypto) => (
          <Card style={{ width: '18rem', margin: '15px' }} key={crypto.id}>
            <Card.Body>
              <Card.Title>{crypto.name}</Card.Title>
              <Card.Text>
              {crypto.current_price}
              {crypto.id}
              </Card.Text>
              <Link to={'/cryptoDetail/'+crypto.id}><Button>Afficher les graphs</Button></Link>
            </Card.Body>
          </Card>
        )))}
      </ul>
    </div>     
  )
}
