import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default function userList() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetchUsers().then((response) => setUsers(response.data.data.users));
      }, []);

    return (
        <div id="userList">
             <ul>
            {users.map(user => (
                <li key={user.id}>
                <p> {user.name} || {user.email}</p>
                </li>
            ))}
            </ul>
        </div>
    )
    
}

async function fetchUsers() {
  return await axios({
    url: 'http://localhost:4000/graphql',
    method: 'post',
    data: {
      query: `
      query {
        users {
          id
          name
          email
        }
      } `
    }
  })
} 
  