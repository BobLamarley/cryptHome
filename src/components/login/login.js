// login.component.js
import React from "react";
//import axios from 'axios';
import { useState, useContext } from 'react';
import { AuthContext } from '../../context';
import './login.css'
import { Grommet, Button, Box, TextInput } from 'grommet';
import { Edit, View, FormLock } from 'grommet-icons';
import { grommet } from "grommet/themes";




export default function Login({ history }) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { setAuthenticated } = useContext(AuthContext)
    const [reveal, setReveal] = useState(false);

  

    function handleLogin() {
      console.log("ici");
       if ( username === 'lama' && password === 'sticot')
       {
            setAuthenticated(true);
            history.push('/');
       }else{
            setAuthenticated(false);
       }    
    }
  
    function createAccount(){
        history.push('/createAccount');
    }

    return (
        <Grommet full theme={grommet}>
            <Box fill align="center" justify="center">
                <Box width="medium">
                    <TextInput id="username" value={username} onChange={({ target: { value } }) => setUsername(value)} placeholder="username"/>
                    <TextInput type={reveal ? "text" : "password"} value={password} onChange={({ target: { value } }) => setPassword(value)} placeholder="password"/>
                    <Button
                    icon={reveal ? <FormLock size="medium" /> : <View size="medium" />}
                    onClick={() => setReveal(!reveal)}
                    />
                    <Button  icon={<Edit />} label="Login" id="loginButton" onClick={()=>handleLogin()}/>
                    <center>Pas de compte ? </center><Button  icon={<Edit />} label="Inscrivez-vous" id="loginButton" onClick={()=>createAccount()}/>
                </Box>
            </Box>
        </Grommet>
    );
}