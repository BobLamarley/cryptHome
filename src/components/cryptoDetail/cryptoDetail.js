import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Loader from 'react-loader-spinner';
import Chart from 'react-apexcharts'
import moment from 'moment';


export default function CryptoDetail(props) {
  const [detail, setDetail] = useState(null)
  const [averagePrice, setAveragePrice] = useState(null)
  const [chartOptions, setChartOptions] = useState(null)
  const [isError, setIsError] = useState(false)


  const populatingHooks = async () => {
    setIsError(false)

    try {
      const response = await fetchCryptoById(props.match.params.cryptoId)
      setDetail(response.data)

      setChartOptions({
        options: {
          stroke: {
            curve: 'smooth'
          },
          markers: {
            size: 0
          },
          xaxis: {
            categories: response.data.prices.map((value) => { return moment.unix(value[0]/1000).format('DD/MM/YYYY hh:mm:ss') }) //xaxis
          }
        },
        series: [{
          data: response.data.prices.map((value) => { return value[1] })//yaxis
        }]

      })

      setAveragePrice(response.data.prices.reduce((previousValue, curentValue) => {
        return previousValue + curentValue[1];
      }, 0) / response.data.prices.length)
      
    } catch (error){
    
      setIsError(true)
    }

  }
  


  useEffect(() => {
    setTimeout(() => populatingHooks(), 2000)
  }, [])


  if (!detail || !chartOptions) {
    return <Loader
         type="Ball-Triangle"
         color="#00BFFF"
         height="100"	
         width="100"/>
  }

  if (isError){
    return <div>Something went wrong ...</div>
  }

  return (
    <div id="cryptoHook">
      <div>
      <p>Current date + price : {moment.unix(detail.prices[0][0]/1000).format('DD/MM/YYYY') }  || { detail.prices[0][1]}€</p>
      <p>Price average on 24h : {averagePrice}</p>
      <Chart options={chartOptions.options} series={chartOptions.series} type="line" width={500} height={320} />
      </div>  
    </div>
  )
}
/** TODO : Export API calls to /services folder 
*/
async function fetchCryptoById(id) {
  return await axios.get(
   'https://api.coingecko.com/api/v3/coins/'+id+'/market_chart?vs_currency=eur&days=1'
  )
}

