FROM node:10.15.1 as build-deps
COPY . .
RUN npm install
RUN npm run build
ENV PORT 5000
RUN npm install -g serve
CMD ["serve", "-s", "build"]
EXPOSE 5000
